<?php

namespace evkv\bxmigrate\repo;

/**
 * Исключение, которое будет выброшено во время работы хранилища миграций.
 */
class Exception extends \evkv\bxmigrate\Exception
{
}
