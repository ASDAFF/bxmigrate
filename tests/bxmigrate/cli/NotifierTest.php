<?php

namespace evkv\bxmigrate\tests\bxmigrate\repo;

use evkv\bxmigrate\tests\BaseCase;
use evkv\bxmigrate\cli\Notifier;

class NotifierTest extends BaseCase
{
    /**
     * @test
     */
    public function testInfo()
    {
        $message = 'message_' . mt_rand();

        $output = $this->getMockBuilder('\\Symfony\\Component\\Console\\Output\\OutputInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $output->expects($this->once())
            ->method('writeln')
            ->with($this->equalTo("<info>{$message}</info>"));

        $notifier = new Notifier($output);
        $notifier->info($message);
    }

    /**
     * @test
     */
    public function testError()
    {
        $message = 'message_' . mt_rand();

        $output = $this->getMockBuilder('\\Symfony\\Component\\Console\\Output\\OutputInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $output->expects($this->once())
            ->method('writeln')
            ->with($this->equalTo("<error>{$message}</error>"));

        $notifier = new Notifier($output);
        $notifier->error($message);
    }
}
